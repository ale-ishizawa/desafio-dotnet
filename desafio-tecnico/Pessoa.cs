﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace desafio_tecnico
{
   public class Pessoa
    {
        private string sexo;
        private int idade;
        private string olhos;
        private string cabelos;

        public Pessoa()
        {
        }

        public Pessoa(string olhos, string cabelos, string sexo, int idade)
        {
            Olhos = olhos;
            Cabelos = cabelos;
            Sexo = sexo;
            Idade = idade;
        }

        public string Olhos { get => olhos; set => olhos = value; }
        public string Cabelos { get => cabelos; set => cabelos = value; }
        public string Sexo { get => sexo; set => sexo = value; }
        public int Idade { get => idade; set => idade = value; }
    }
}
