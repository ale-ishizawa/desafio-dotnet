﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace desafio_tecnico
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcao;

            do
            {
                Console.WriteLine("\n[ 1 ] Exercício 1");
                Console.WriteLine("[ 2 ] Exercício 2");
                Console.WriteLine("[ 3 ] Exercício 3");
                Console.WriteLine("[ 4 ] Exercício 4");
                Console.WriteLine("[ 5 ] Exercício 5");
                Console.WriteLine("[ 7 ] Exercício 7");
                Console.WriteLine("[ 8 ] Exercício 8");
                Console.WriteLine("Escolha um exercício.");
                opcao = Int32.Parse(Console.ReadLine());
                switch (opcao)
                {
                    case 1:
                        exerc1();
                        break;
                    case 2:
                        exerc2();
                        break;
                    case 3:
                        exerc3();
                        break;
                    case 4:
                        exerc4();
                        break;
                    case 5:
                        exerc5();
                        break;
                    case 7:
                        exerc7();
                        break;
                    case 8:
                        exerc8();
                        break;
                    default:
                        sairPrograma();
                        break;
                }
            }
            while (opcao != 0);
        }
        
        public static void exerc1()
        {
            Console.Write("Informe o número do mês: ");
            int mes = Int32.Parse(Console.ReadLine()); 
            if(mes > 0 && mes < 13)
            {
                string mesExtenso = new DateTimeFormatInfo().GetMonthName(mes);
                Console.Clear();
                Console.WriteLine("O mês em extenso é: " + mesExtenso);
            }
            else
            {
                Console.WriteLine();
                Console.Clear();
                Console.WriteLine("Informe um número entre 1 e 12");
            }
        }

        public static void exerc2()
        {
            int[] arrayNumbers = new int[3];
            Console.WriteLine("Informe o primeiro número: ");
            arrayNumbers[0] = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Informe o segudo número: ");
            arrayNumbers[1] = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Informe o terceiro número: ");
            arrayNumbers[2] = Int32.Parse(Console.ReadLine());

            int[] sortedArray = (from element in arrayNumbers orderby element descending select element).ToArray();
            Console.Clear();
            Console.WriteLine("Os números na ordem decrescente são: " + sortedArray[0] + ", " + sortedArray[1] + ", " + sortedArray[2]);
        }

        public static void exerc3()
        {
            int[] inteiros = new int[500];
            int media = 0;
            Random random = new Random();
            for (int i = 0; i < 500; i++)
            {
               
                inteiros[i] = random.Next(1, 1000);
                media += inteiros[i];
            }

            int maxValue = inteiros.Max();
            int minValue = inteiros.Min();
            int mediaValue = media / 500;

            Console.Clear();
            Console.WriteLine("Maior Valor: " + maxValue);
            Console.WriteLine("Menor Valor: " + minValue);
            Console.WriteLine("Média dos números lidos: " + mediaValue);
        }

        public static void exerc4()
        {
            Console.Clear();
            
            decimal[] salarios = new decimal[100];
            decimal mediaSalarial = 0;
            int i = 0;
            string[] funcionarios = new string[100];
            Console.WriteLine("Informe o nome do Funcionário: ");
            string nome = Console.ReadLine();
            funcionarios[i] = nome;            
            Console.WriteLine("Informe o salário do funcionário: ");
            decimal salario = Decimal.Parse(Console.ReadLine());
            salarios[i] = salario;
            mediaSalarial += salario;
            i++;
            //Pergunta se deseja cadastrar mais funcionários
            Console.WriteLine("Deseja cadastrar mais funcionários ? s/n");
            string opcao = Console.ReadLine();
            while(opcao == "s" || opcao == "S")
            {
                Console.WriteLine("Informe o nome do Funcionário: ");
                nome = Console.ReadLine();
                funcionarios[i] = nome;
                Console.WriteLine("Informe o salário do funcionário: ");
                salario = Decimal.Parse(Console.ReadLine());
                salarios[i] = salario;
                mediaSalarial += salario;
                i++;
                Console.WriteLine("Deseja cadastrar mais funcionários ? s/n");
                opcao = Console.ReadLine();
                if(opcao == "n" || opcao == "N") { break; }
            }

            //Obtém o maior salário, o menor e a média salarial
            decimal maxValue = salarios.Max();
            decimal minValue = 0;
            //Obtém o menor valor
            for (int aux = 0; aux < i; aux++)
            {
                if(aux == 0) { minValue = salarios[aux]; }

                if (minValue > 0 && minValue > salarios[aux])
                {
                    minValue = salarios[aux];
                }
            }
            //decimal  = salarios.Min();
            decimal mediaValue = mediaSalarial / i;
            Console.Clear();
            Console.WriteLine("Maior Valor: " + maxValue);
            Console.WriteLine("Menor Valor: " + minValue);
            Console.WriteLine("Média Salarial: " + mediaValue);
        }

        public static void exerc5()
        {
            string opcao = "";
            Console.Clear();
            Console.WriteLine("Deseja cadastrar uma pessoa? s/n");
            opcao = Console.ReadLine();
            List<Pessoa> _pessoas = new List<Pessoa>();
            Pessoa pes;
            do
            {
                Console.WriteLine("Informe o sexo da pessoa: M ou F ?");
                string sexo = Console.ReadLine();
                Console.WriteLine("Informe a cor dos olhos: azuis, verdes ou castanhos ?");
                string olhos = Console.ReadLine();
                Console.WriteLine("Informe a cor dos cabelos da pessoa: louros, castanhos ou pretos? ");
                string cabelos = Console.ReadLine();
                Console.WriteLine("Informe a idade da pessoa.");
                int idade= Int32.Parse(Console.ReadLine());

                pes = new Pessoa(olhos, cabelos, sexo, idade);
                _pessoas.Add(pes);

                Console.Clear();
                Console.WriteLine("Deseja cadastrar outra pessoa? s/n");
                opcao = Console.ReadLine();
                if(opcao == "n" || opcao == "N") { break; }
            }
            while (opcao == "s" || opcao == "S");

            Console.Clear();
            int maiorIdade = 0;
            int qtdMulheres = 0;
            int qtdVerdes = 0;
            foreach(Pessoa p in _pessoas)
            {
                if(maiorIdade < p.Idade)
                {
                    maiorIdade = p.Idade;
                }
                if (p.Idade > 17 && p.Idade < 36 && p.Sexo == "F")
                {
                    qtdMulheres++;
                }
                if(p.Olhos == "verdes" && p.Cabelos == "louros")
                {
                    qtdVerdes++;
                }
            }

            Console.WriteLine("Maior idade dos habitantes: " + maiorIdade);
            Console.WriteLine("Quantidade de indivíduos do sexo feminino cuja idade estão entre 18 e 35 anos: " + qtdMulheres);
            Console.WriteLine("Quantidade de indivíduos que tenham olhos verdes e cabelos louros: " + qtdVerdes);

        }

        public static void exerc7()
        {
            decimal[,] matrizVendas = new decimal[3, 5];
            decimal[] vendedor1 = new decimal[5]{ 15000, 25000, 235000, 175000, 80000 };
            decimal[] vendedor2 = new decimal[5] { 9800, 57000, 130000, 201000, 102100 };
            decimal[] vendedor3 = new decimal[5] { 42000, 80000, 142000, 241500, 330000 };

            for(int i = 0; i < 3; i++)
            {
                for(int j = 0; j < 5; j++)
                {
                    if(i == 0)
                    {
                        matrizVendas[i, j] = vendedor1[j];
                    }
                    if(i == 1)
                    {
                        matrizVendas[i, j] = vendedor2[j];
                    }
                    if(i == 2)
                    {
                        matrizVendas[i, j] = vendedor3[j];
                    }
                }
            }

            //Faturamento Diário
            decimal[] faturamentoDiario = new decimal[5];
            //Faturamento Semanal
            decimal faturamentoSemanal = 0;
            //Faturamento Por Vendedor
            decimal[] faturamentoVendedores = new decimal[3];
            for (int a = 0; a < 3; a++)
            {
                for(int b = 0; b < 5; b++)
                {
                    faturamentoDiario[b] += matrizVendas[a, b];
                    faturamentoSemanal += matrizVendas[a, b];
                    if(a == 0)
                    {
                        faturamentoVendedores[a] += matrizVendas[a, b];
                    }
                    if(a == 1)
                    {
                        faturamentoVendedores[a] += matrizVendas[a, b];
                    }
                    if(a == 2)
                    {
                        faturamentoVendedores[a] += matrizVendas[a, b];
                    }
                }
            }
            Console.Clear();
            Console.WriteLine("Faturamento diário da empresa: \n" +
                "Segunda-Feira: "+faturamentoDiario[0] + " \n" +
                "Terça-Feira: " + faturamentoDiario[1] + " \n" +
                "Quarta-Feira: " + faturamentoDiario[2] + " \n " +
                "Quinta-Feira: " + faturamentoDiario[3] + "\n" + 
                "Sexta-Feira: " + faturamentoDiario[4] + " \n");
            Console.WriteLine("Faturamento Semanal da empresa: " + faturamentoSemanal);
            Console.WriteLine("Faturamento Semanal por vendedor: \n" +
                "Vendedor 1: " + faturamentoVendedores[0] + "\n" +
                "Vendedor 2: " + faturamentoVendedores[1] + "\n" +
                "Vendedor 3: " + faturamentoVendedores[2] + "\n");
            Console.WriteLine("Vendedor com o maior faturamento: " + faturamentoVendedores.Max());
            Console.ReadKey();

        }

        public static void exerc8()
        {
            int[,] matriz = new int[5, 5];
            Random ran = new Random();
            //Preenchendo matriz
            for (int a = 0; a < 5; a++)
            {
                for (int b = 0; b < 5; b++)
                {                   
                    matriz[a, b] = ran.Next(1, 99);
                }
            }

            //Soma da Linha 4
            int somaLinha4 = 0;
            for(int c = 0; c < 5; c++)
            {
                somaLinha4 += matriz[3, c];
            }

            //Soma da coluna 2
            int somaCol2 = 0;
            for(int l = 0; l < 5; l++)
            {
                somaCol2 += matriz[l, 1];
            }

            //Soma da diagonal principal
            int somaDiagPrincipal = 0;
            for(int l = 0; l < 5; l++)
            {
                somaDiagPrincipal += matriz[l, l];                
            }

            //Soma da diagonal secundaria
            int somaDiagSecundaria = 0;
            for (int l = 4; l >= 0; l--)
            {
                somaDiagSecundaria += matriz[l, l];
            }

            //Soma da Matriz
            int somaMatriz = 0;
            for (int a = 0; a < 5; a++)
            {
                for (int b = 0; b < 5; b++)
                {
                    somaMatriz += matriz[a, b];
                }
            }

            Console.Clear();
            Console.WriteLine("Soma da Linha 4: " + somaLinha4);
            Console.WriteLine("Soma da Coluna 2: " + somaCol2);
            Console.WriteLine("Soma da Diagonal Principal: " + somaDiagPrincipal);
            Console.WriteLine("Soma da Diagonal Secundária: " + somaDiagSecundaria);
            Console.WriteLine("Soma da Matriz: " + somaMatriz);
            Console.ReadKey();

        }
        public static void sairPrograma()
        {

        }
    }
}
